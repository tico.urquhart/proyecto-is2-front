import { combineReducers } from 'redux';

import { authReducer } from './authReducer';
import { rolesReducer } from './rolesReducer';

export const rootReducer = combineReducers({
    auth: authReducer,
    roles: rolesReducer
})