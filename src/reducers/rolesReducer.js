import { types } from '../types/types';

const initialState = {
    
    // name: null
}

export const rolesReducer = ( state = initialState, action ) => {

    switch ( action.type ) {
        
        case types.rolesList:
            return {
                ...action.payload
            }

        default:
            return state;
    }

}