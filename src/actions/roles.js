import {  fetchSinToken,fetchConToken } from '../helpers/fetch';
import { types } from '../types/types';
import Swal from 'sweetalert2';

export const rolesList = () => {
    return async( dispatch ) => {
        const roles = [
            {
                "id": 1,
                "descripcion": "seguridad"
            },
            {
                "id": 2,
                "descripcion": "proyecto"
            },
            {
                "id": 3,
                "descripcion": "desarrollo"
            }
        ];
        // const resp =await fetchConToken( 'logueadoModulos' ) ;
        // const body =  await resp.json();
        // const listaRol = await body;
        // console.log(listaRol);

        dispatch( insRol({
            roles
        }) )
    }
}

const insRol = ( roles ) => ({
    type: types.rolesList,
    payload: roles
});
