import { fetchSinToken, fetchConToken } from '../helpers/fetch';
import { types } from '../types/types';
import Swal from 'sweetalert2';



export const startLogin = ( email, pass ) => {
    return async( dispatch ) => {

        const resp = await fetchSinToken( 'login', { email, pass }, 'POST' );
        const body = await resp.json();

        
        if( body.cod === '00' ) {

            localStorage.setItem('token', body.token );
            localStorage.setItem('token-init-date', new Date().getTime() );
            

            dispatch( login({
                uid: body.usuario.id,
                name: body.usuario.name
            }) )
        } else {
            Swal.fire('Error', body.message, 'error');
        }
        

    }
}

export const startRegister = ( email, pass, name ) => {
    return async( dispatch ) => {

        const resp = await fetchSinToken( 'auth/new', { email, pass, name }, 'POST' );
        const body = await resp.json();

        if( body.cod === '00' ) {
            localStorage.setItem('token', body.token );
            localStorage.setItem('token-init-date', new Date().getTime() );

            dispatch( login({
                
            }) )
        } else {
            Swal.fire('Error', body.msg, 'error');
        }


    }
}

export const startChecking = () => {
    return async(dispatch) => {

        const resp = await fetchConToken( 'auth/renew' );
        const body =  resp;
       // console.log( {...body, prueba: 2} ) ;
        if( body.cod === '00') {
            localStorage.setItem('token', body.token );
            localStorage.setItem('token-init-date', new Date().getTime() );

            dispatch( login({
                uid: body.uid,
                name: body.name
            }) )
        } else {
            dispatch( checkingFinish() );
        }
    }
}

const checkingFinish = () => ({ type: types.authCheckingFinish });



const login = ( user ) => ({
    type: types.authLogin,
    payload: user
});


export const startLogout = () => {
    return ( dispatch ) => {

        localStorage.clear();
        dispatch( logout() );
    }
}

const logout = () => ({ type: types.authLogout })