import React,{useState,useEffect} from 'react'
import { useDispatch } from 'react-redux';
import { listarModulos } from '../../actions/modulo';
import FilaModulo from './FilaModulo'
import FormModulo from './FormModulo';
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'
import { fetchGuardar } from '../../helpers/fetch';
//import React from 'react' // IMPORTAR DE AQUI EL FETCH CON TOKEN

export const Panel = () => {
    const [modal, setmodal] = useState();
    const [modoForm,setModoForm] = useState('N');
    const [lista,setLista] = useState({'lista':[]});
    const [select,setSelect] = useState({'id':'0'});
    const dispatch = useDispatch();
    const MySwal = withReactContent(Swal)
    const buscar= async (e)=>{
        e.preventDefault()
        const buscar = e.target[0].value
        setSelect({"id":'0'});  
        listar(buscar );
    }
    const listar = async (valor)=>{
        const datos = await listarModulos(valor);
        setLista(datos)
    }
    useEffect(()=>{
        listar();
        console.log(lista)
    },[]);

    const seleccionarFila = (id)=>{
        console.log('id',id);
        setSelect(id);
    }
    const nuevoModal = ()=>{
        setmodal(1);
        // console.log("Abrir formulario nuevo rol")
    }
    const modificarModal = ()=>{
        console.log("Abrir formulario modificar modulo")
    }
    const eliminarAlert = ()=>{
        console.log(select);
        fetchGuardar("modulo/"+select,{},"E")
    }
    const cerrarModal = () => {
        setmodal(0)
    }
    
    const sweetAlertEliminar = () => {
        if(select.id!=='0'){
            Swal.fire({
                title: 'Esta seguro que desea eliminar?',
                showDenyButton: true,  showCancelButton: true,
                confirmButtonText: `Confirmar`,
                denyButtonText: `No confirmar`,
              }).then((result) => {
                  /* Read more about isConfirmed, isDenied below */
                  if (result.isConfirmed) {
                      Swal.fire('Eliminado Correctamente!', '', 'success')
                      eliminarAlert();
                      setSelect({'id':'0'});
                  }
              });
            
        }

    }


  return (
    <>
        <div className="container-fluid">
            <div className="row">
                <h1 style={{color:'white'}}>Modulos</h1>
            </div>
            <div className="row">
            <div className="col-sm-6">
                        <form onSubmit={buscar}>
                            <div className="row">
                                <div className="col-sm-8">
                                    <input type="text" className="form-control" placeholder="Buscar..."/>
                                </div>
                                <div className="col-sm-4">
                                    <button className="btn-primary btn" type='submit'>Buscar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                <div className="col-sm-6 text-end">
                    <button className="btn btn-success" onClick={()=>{nuevoModal()}}>Nuevo</button>
                    <button className="btn btn-secondary" onClick={()=>{modificarModal()}}>Modificar</button>
                    <button className="btn btn-danger" onClick={()=>{sweetAlertEliminar()}}>Eliminar</button>
                </div>
            </div>
            <div className="row">
                <table className="table table-striped table-hover" style={{backgroundColor:"#ffffff"}}>
                    <thead>
                        <tr style={{backgroundColor:'darkblue',color:'white'}}><th>id</th><th>Descripción</th></tr>
                    </thead>
                    <tbody>
                        {lista.lista.map(
                                (fila,pos)=>{
                                    return (<tr key={fila.id+"f"} onClick={()=>{seleccionarFila(fila.id)}}><FilaModulo key={fila.id} descripcion={fila.descripcion} observacion=""/></tr>);
                                })
                        }
                    </tbody>
                </table>
            </div>
            { (modal===1) && <FormModulo modo={modoForm} seleccionado={select.id}  funcCerrar = {cerrarModal}/> }

            {/* { (modal===1) && <FormModulo funcCerrar = {cerrarModal} /> } */}
        </div>
    </>
  )
}
