import React, { Component } from "react";
import 'bootstrap/dist/css/bootstrap.css';
import { Button, Modal, ModalHeader, ModalBody } from "reactstrap";
import { fetchGuardar } from "../../helpers/fetch";

class FormModulo extends Component {

    constructor(props) {
        super(props)
        this.cerrar = this.props.funcCerrar;
        this.seleccionado = this.props.seleccionado;
        this.modo = this.props.modo;
        this.state={
            NombreModulo:'',
            abierto: true,
        }

        this.changeNombreModuloHandler=this.changeNombreModuloHandler.bind(this);
        this.guardar=this.guardar.bind(this);
    }

    abrirModal=()=>{
        this.setState({
            abierto: !this.state.abierto
        });
    }

    changeNombreModuloHandler=(event)=>{
        this.setState({
            NombreModulo: event.target.value
        })
    }

    guardar=(e)=>{
        e.preventDefault();
        let modulo={NombreModulo: this.state.NombreModulo, email: this.state.email, contra: this.state.contra, ConfirContra: this.state.ConfirContra, rol: this.state.rol};
        console.log('modulo=>'+JSON.stringify(modulo));
        alert('Modulo creado con éxito!')
    }

    cancel(){
        this.props.history.push();
    }
    guardar = async (e)=>{
        e.preventDefault();
        let temp ;
        let modulo={
        	"descripcion":this.state.NombreModulo
        }
        console.log('usuario=>'+JSON.stringify(modulo));
        if(this.modo==='M'){
            temp= await (await fetchGuardar('modulo/'+this.seleccionado,modulo , this.modo)).json()

        }else{
            console.log(this.modo);
            temp= await (await fetchGuardar('modulo',modulo , this.modo)).json()

        }
        console.log(temp);
        alert('Modulo creado con éxito!')
    }

    render() {
        return(
            <div>
            <div className="container">
                <div className="row">
                    <Modal isOpen={this.state.abierto}>
                    <ModalHeader>
                    <h3 className="text-center">Creación de módulo</h3>
                    </ModalHeader>
                    <ModalBody>
                    <div className="col-md-6 offset-md-3 offset-md-3">
                        <div className="card-body">
                            <form>
                                <div className="form-group">
                                    <label>Nombre del módulo: *</label>
                                    <br></br>
                                    <br></br>
                                    <input placeholder="Nombre de modulo" name="NombreModulo" className="form-control" value={this.state.NombreModulo} onChange={this.changeNombreModuloHandler}/>
                                    <br></br>
                                    <br></br>
                                    <br></br>
                                </div>
                                <button className="btn btn-success" onClick={this.guardar}>Guardar</button>
                                <button className="btn btn-danger" onClick={this.cerrar} style={{marginLeft:"25px"}}>Cerrar</button>
                            </form>
                        </div>
                    </div>
                    </ModalBody>
                    </Modal>
                </div>
            </div>   
            </div>
        )
    }
}

export default FormModulo
