import { useState } from "react";
import 'bootstrap/dist/css/bootstrap.css';
import { Button, Modal, ModalHeader, ModalBody } from "reactstrap";

const FormModulo1 = () =>{

    const [modulo, setModulo] = useState({
        NombreModulo:''
    });

    const [abierto, setAbierto] = useState(true);
    
    const changeModulolHandler = (event) => {
        setModulo({
            ...modulo,
            [event.target.name]:event.target.value

        })
    };

    const abrirModal=()=>{
        setAbierto({
            abierto: !abierto
        });
    }

    const guardar = async (e)=>{
        e.preventDefault();
        //let temp ;
        console.log('NombreModulo:'+modulo.NombreModulo)
        /*if(modo==='M'){
            temp= await (await fetchGuardar('usuario/'+seleccionado,usuario , modo)).json()

        }else{
            console.log(modo);
            temp= await (await fetchGuardar('usuario',usuario , modo)).json()

        }
        console.log(temp);*/
        alert('Modulo creado con éxito!')
    }

    const cancel=()=>{
        this.props.history.push();
    }

    return(
        <div>
        <div className="container">
            <div className="row">
                <Button color="success" onClick={abrirModal}>Crear nuevo módulo</Button>
                <Modal isOpen={abierto}>
                <ModalHeader>
                <h3 className="text-center">Creación de módulo</h3>
                </ModalHeader>
                <ModalBody>
                <div className="col-md-6 offset-md-3 offset-md-3">
                    <div className="card-body">
                        <form>
                            <div className="form-group">
                                <label>Nombre del módulo: *</label>
                                <br></br>
                                <br></br>
                                <input placeholder="Nombre de modulo" name="NombreModulo" className="form-control" onChange={changeModulolHandler}/>
                                <br></br>
                                <br></br>
                                <br></br>
                            </div>
                            <button className="btn btn-success" onClick={guardar}>Guardar</button>
                            <button className="btn btn-danger" onClick={cancel} style={{marginLeft:"25px"}}>Cerrar</button>
                        </form>
                    </div>
                </div>
                </ModalBody>
                </Modal>
            </div>
        </div>   
        </div>
    )

}

export default FormModulo1