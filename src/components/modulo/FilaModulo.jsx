import React from 'react'
import PropTypes from 'prop-types'

const FilaModulo = ({descripcion,observacion}) => {
  return (
        <>
            <td>{descripcion}</td>
            <td>{observacion}</td>
        </>
  )
}

FilaModulo.propTypes = {
    descripcion: PropTypes.string,
    observacion: PropTypes.string

}

export default FilaModulo
