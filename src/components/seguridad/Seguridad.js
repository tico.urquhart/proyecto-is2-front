import React, {memo } from 'react'
import Card from '../card/Card'


export const Seguridad = memo(({setMenu}) => {
  return (
      <div className='container-fluid'>
        <div className='row'>
            <Card setMenu={setMenu} nombre='Usuarios' observacion='usuarios' className="col"/>
            <Card setMenu={setMenu} nombre='Roles' observacion='roles' className="col"/>
            <Card setMenu={setMenu} nombre='Modulos' observacion='modulos' className="col"/>

        </div>

      </div>

  )
})
