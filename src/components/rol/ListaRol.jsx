import React,{useState,useEffect} from 'react'
import { fetchGuardar,fetchObtenerDatos } from '../../helpers/fetch';
import { MultiSelect } from "react-multi-select-component";

export const ListaRol = () => {

    const tmp_permisos = []

    const [selected, setSelected] = useState([]);
    const [opciones,setOpciones] = useState([])

    for(let i=0; i<selected.length; i++){
        tmp_permisos.push(selected[i].value)
    }

    this.state.permisos=tmp_permisos;

    const getListaModulo = async () =>{
        let temp = await (await fetchObtenerDatos('modulo')).json()
        console.log(temp);
        let listaTemp =[]
        temp.lista.forEach((item, i) => {
            listaTemp.push({'label':item.descripcion, 'value':item.id})
        });

        this.setOpciones({
            listaModulos:listaTemp
        });
        console.log(this.state.listaModulos);
        return listaTemp
    }
    useEffect(()=>{
        getListaModulo();
    },[]);

    return (
        <div>
        <MultiSelect
            options={opciones}
            value={selected}
            onChange={setSelected}
            labelledBy="Select"
            hasSelectAll={false}
        />
        </div>
    );
}
