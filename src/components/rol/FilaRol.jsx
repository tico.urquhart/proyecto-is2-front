import React from 'react'
import PropTypes from 'prop-types'

const FilaRol = ({descripcion,observacion}) => {
  return (
        <>
            <td>{descripcion}</td>
            <td>{observacion}</td>
        </>
  )
}

FilaRol.propTypes = {
    descripcion: PropTypes.string,
    observacion: PropTypes.string

}

export default FilaRol
