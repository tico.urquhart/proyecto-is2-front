import React, { Component, useState , useEffect} from "react";
import 'bootstrap/dist/css/bootstrap.css';
import { Button, Modal, ModalHeader, ModalBody } from "reactstrap";
import { fetchConToken,fetchObtenerDatos,fetchGuardar } from "../../helpers/fetch";
import { MultiSelect } from "react-multi-select-component";
import { ListaRol } from './ListaRol'

class FormRol extends Component {
    constructor(props) {
        super(props)
        this.cerrar = this.props.funcCerrar;
        this.seleccionado = this.props.seleccionado;
        this.modo = this.props.modo;
        this.state={
            NombreRol:'',
            ObsRol:'',
            permisos:[],
            abierto: true,
            listaModulos : [{'value':'1','label':'Seguridad'},{'value':'2','label':'Proyecto'},{'value':'3','label':'Desarrollo'}]
        }
        this.getListaModulo()

        this.changeNombreRolHandler=this.changeNombreRolHandler.bind(this);
        this.changeObsRolHandler=this.changeObsRolHandler.bind(this);
        this.changepermisosHandler=this.changepermisosHandler.bind(this);
        this.guardar=this.guardar.bind(this);
    }

    abrirModal=()=>{
        this.setState({
            abierto: !this.state.abierto
        });
    }

    changeNombreRolHandler=(event)=>{
        this.setState({
            NombreRol: event.target.value
        })
    }

    changeObsRolHandler=(event)=>{
        this.setState({
            ObsRol: event.target.value
        })
    }

     changepermisosHandler=(event)=>{
        this.setState({
            rol: event.target.value
        })
    }
    getListaModulo = async () =>{
        let temp = await (await fetchObtenerDatos('modulo')).json()
        console.log(temp);

        this.setState({
            listaModulos:temp.lista
        });
        console.log(this.state.listaModulos);
    }

    guardar=async (e)=>{
        e.preventDefault();
        let  valores = {NombreRol: this.state.NombreRol, ObsRol: this.state.ObsRol, permisos: []};
        let temporal;
        const temp = Array.from(e.target.childNodes[2].childNodes[1].childNodes[2].childNodes)
        let permiso = [];
        permiso = temp.filter((elemento)=> ( elemento.childNodes[0].childNodes[0].childNodes[0].checked))
        permiso.forEach((elemento, i) => {
            valores.permisos.push({'modulo':elemento.childNodes[0].childNodes[0].childNodes[0].value,'acceso':'1'})
        });

        if(this.modo==='M'){
            temporal= await (await fetchGuardar('rol/'+this.seleccionado,valores , this.modo)).json()

        }else{
            console.log(this.modo);
            temporal= await (await fetchGuardar('rol',valores , this.modo)).json()
        }
    }




    // <Button color="success" onClick={this.abrirModal}>Crear nuevo rol</Button>
    render() {
        return(
            <div>
            <div className="container">
                <div className="row">
                    <Modal isOpen={this.state.abierto}>
                    <ModalHeader>
                        <h3 className="text-center">Creación de rol</h3>
                    </ModalHeader>
                    <ModalBody>
                    <div className="col-md-6 offset-md-3 offset-md-3">
                        <div className="card-body">
                            <form onSubmit={this.guardar}>
                                <div className="form-group">
                                    <label>Nombre del rol:</label>
                                    <input placeholder="Nombre del rol" name="NombreRol" className="form-control" value={this.state.NombreRol} onChange={this.changeNombreRolHandler}/>
                                    <br></br>
                                </div>
                                <div className="form-group">
                                    <label>Observación:</label>
                                    <textarea name="ObsRol" className="form-control" value={this.state.ObsRol} onChange={this.changeObsRolHandler}></textarea>
                                    <br></br>
                                </div>
                                <div className="form-group">
                                    <label>Permisos:</label>
                                    <table className="table table-striped table-hover" style={{width:"100%"}}>
                                        <colgroup>
                                            <col span="1" style={{width:"15%"}}/>
                                            <col span="1" style={{width:"85%"}}/>
                                        </colgroup>
                                        <thead>
                                            <tr>
                                                <th scope="col"> </th>
                                                <th scope="col">Modulo</th>
                                            </tr>
                                        </thead>
                                        <tbody id='tabla-form'>
                                            {this.state.listaModulos.map((item,pos)=>{
                                                return <tr>
                                                    <td>
                                                        <div className="form-check">
                                                            <input className="form-check-input" type="checkbox" value={item.id} id={item.id}/>
                                                        </div>
                                                    </td>
                                                    <td>{item.descripcion}</td>
                                                </tr>
                                            })}
                                        </tbody>
                                    </table>
                                    <br></br>
                                </div>
                                <button className="btn btn-success" >Guardar</button>
                                <button className="btn btn-danger" onClick={this.cerrar} style={{marginLeft:"25px"}}>Cerrar</button>
                            </form>
                        </div>
                    </div>
                    </ModalBody>
                    </Modal>
                </div>
            </div>
            </div>
        )
    }
}

export default FormRol
