import React, { useState } from "react";
import 'bootstrap/dist/css/bootstrap.css';
import { Button, Modal, ModalHeader, ModalBody } from "reactstrap";
import { MultiSelect } from "react-multi-select-component";

const FormRol1 = () => {

    const [rol, setRol] = useState({
        NombreRol:'',
        ObsRol:''
    });

    const listaPermisos = [
        {label:'Seguridad', value:1},
        {label:'Proyecto', value:2},
        {label:'Desarrollo', value:3}
    ];

    const [abierto, setAbierto] = useState(true);
    const [permisos, setPermisos] = useState([]);

    const changeRolHandler = (event) => {
        setRol({
            ...rol,
            [event.target.name]:event.target.value

        })
    };

    const abrirModal=()=>{
        setAbierto({
            abierto: !abierto
        });
    }

    const guardar = async (e)=>{
        e.preventDefault();
        //let temp ;
        let idPermisos = []
         for (let i = 0; i < permisos.length; i++) {
            idPermisos.push(permisos[i].value)
        }
        console.log('Nombre:'+rol.NombreRol+' Observacion:'+rol.ObsRol+' Permisos:'+idPermisos)
        /*if(modo==='M'){
            temp= await (await fetchGuardar('usuario/'+seleccionado,usuario , modo)).json()

        }else{
            console.log(modo);
            temp= await (await fetchGuardar('usuario',usuario , modo)).json()

        }
        console.log(temp);*/
        alert('Rol creado con éxito!')
    }

    const cancel=()=>{
        this.props.history.push();
    }

    return(
        <div>
        <div className="container">
            <div className="row">
                <Button color="success" onClick={abrirModal}>Crear nuevo rol</Button>
                <Modal isOpen={abierto}>
                <ModalHeader>
                <h3 className="text-center">Creación de rol</h3>
                </ModalHeader>
                <ModalBody>
                <div className="col-md-6 offset-md-3 offset-md-3">
                    <div className="card-body">
                        <form>
                            <div className="form-group">
                                <label>Nombre del rol:</label>
                                <input placeholder="Nombre del rol" name="NombreRol" className="form-control" onChange={changeRolHandler}/>
                                <br></br>
                            </div>
                            <div className="form-group">
                                <label>Observación:</label>
                                <textarea name="ObsRol" className="form-control" onChange={changeRolHandler}></textarea>
                                <br></br>
                            </div>
                            <div className="form-group">
                                <label>Permisos:</label>
                                
                                <MultiSelect
                                    hasSelectAll={false}
                                    options={listaPermisos}
                                    value={permisos}
                                    onChange={setPermisos}
                                    labelledBy="Select"
                                />

                                <br></br>
                            </div>
                            <button className="btn btn-success" onClick={guardar}>Guardar</button>
                            <button className="btn btn-danger" onClick={cancel} style={{marginLeft:"25px"}}>Cerrar</button>
                        </form>
                    </div>
                </div>
                </ModalBody>
                </Modal>
            </div>
        </div>   
        </div>
        )

}

export default FormRol1