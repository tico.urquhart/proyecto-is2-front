import React, { useCallback, useContext, useEffect, useState } from 'react'
import Navbar from '../navbar/Navbar'
import { Seguridad } from '../seguridad/Seguridad';
import { Slidebar } from '../slidebar/Slidebar'
import { Panel as PanelProyecto } from '../proyecto/Panel'
import { Panel as PanelUsuario } from '../usuario/Panel';
import { Panel as PanelRol } from '../rol/Panel';
import { Panel as PanelModulo} from '../modulo/Panel';

export const Home = () => {
  const [menu, setMenu] = useState(0);

  useEffect(() => {
    console.log(menu)
  }, [menu])

  const setSubMenu = useCallback( (num) =>{
    setMenu( num);
}, [setMenu])
  return (
    <>
        <header style={{width:'100%'}}>
        <Navbar />
        </header>
          <div className="container-fluid" style={{display: 'flex',   flexWrap: 'nowrap',   height: '100vh',   paddingLeft: '0px'}} >
          <Slidebar  setMenu={setSubMenu}/>
          {(menu===1) && <Seguridad setMenu={setSubMenu}/>}
          {(menu===2) && <PanelProyecto setMenu={setSubMenu} />}
          {(menu===3) && (<h1>Desarrollo</h1>)}
          {(menu===11) && <PanelUsuario />}
          {(menu===12) && <PanelRol />}
          {(menu===13) && <PanelModulo />}
        </div>

    </>
  )
}
