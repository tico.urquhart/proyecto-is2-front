import React, {useState,useEffect} from 'react'
import OpcionGrupoMenu from './OpcionGrupoMenu';
import style from './sidebars.css';


const Sidebar = () => {
    const [lista,setLista] = React.useState([
	{
		"id": 1,
		"descripcion": "seguridad"
	},
	{
		"id": 2,
		"descripcion": "proyecto"
	},
	{
		"id": 3,
		"descripcion": "desarrollo"
	}
]);
    const obtenerOpciones = async ()=>{
        const crudo = await fetch( 'http://127.0.0.1:8000/api/modulosRol/', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + 'test',
            },
        } ) ;
        const datos = await crudo.json();
        setLista(datos);
       // console.log(datos)
    }
    // useEffect(()=>{
    //     console.log("Testing")
    //     obtenerOpciones()
    // },[])
    return (
    <div className="container-fluid" style={{display: 'flex',   flexWrap: 'nowrap',   height: '100vh',   paddingLeft: '0px'}}>
        <div className="d-flex flex-column flex-shrink-0 p-3 text-white bg-dark" style={{width: '280px'}}>
            <ul className="nav nav-pills flex-column mb-auto">
                {lista.map(
                    (valor)=>{
                            console.log(valor.id , valor.descripcion)
                                return(
                                    <>
                                    <OpcionGrupoMenu key={valor.id} descripcion={valor.descripcion} />
                                    <hr/>
                                    </>
                                    )
                            }
                )}
            </ul>
        </div>
    </div>
  )
}

export default Sidebar
