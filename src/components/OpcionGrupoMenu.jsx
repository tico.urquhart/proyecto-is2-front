import React from 'react'
import PropTypes from 'prop-types'

const  OpcionGrupoMenu= ({descripcion}) => {
  return (
      <li className="nav-item">
          <a href="#" onClick={()=>{}} className="nav-link bg-primary text-white" aria-current="page">
              {descripcion}
          </a>
      </li>

  )
}

OpcionGrupoMenu.propTypes = {
    descripcion: PropTypes.string
};

export default OpcionGrupoMenu
