import React, { useState, useEffect } from "react";
import 'bootstrap/dist/css/bootstrap.css';
import { Button, Modal, ModalHeader, ModalBody } from "reactstrap";
import { fetchConToken,fetchObtenerDatos,fetchGuardar } from "../../helpers/fetch";

const FormProyecto = ({modo,seleccionado,funcCerrar}) => {

    const [abierto, setAbierto] = useState(true);
    const [proyecto,setProyecto] = useState({'descripcion':"","observacion":"","estado":""})

    const cargarDatos = async()=>{
        if(seleccionado!=0 && modo == 'M'){
            let temp = await (await fetchObtenerDatos("proyecto/id",seleccionado )).json()
            console.log(temp);
            setProyecto(temp.proyecto)
        }
    }
    useEffect(()=>{
        cargarDatos();

    },[])
    const handleChange = (e) => {
        let temp = {... proyecto}
        console.log(e.target.name,temp , e.target.value)
        temp[e.target.name] = e.target.value
        setProyecto(temp)

    };
     const abrirModal=()=>{
        setAbierto({
            abierto: !abierto
        });
    }


    const guardar = async (e)=>{
        e.preventDefault();
        let temp ;
        console.log('e',e)
        if(modo==='M'){
            let proyecto = {'descripcion':e.target[0].value,'observacion':e.target[1].value,'estado':e.target[2].value}
            console.log(seleccionado,proyecto,modo)
            temp= await (await fetchGuardar('proyecto/'+seleccionado,proyecto , modo)).json()
            console.log(temp);
        }else{
            let proyecto = {'descripcion':e.target[0].value,'observacion':e.target[1].value}
            console.log(modo);
            temp= await (await fetchGuardar('proyecto',proyecto , modo)).json()

        }
        funcCerrar()
        console.log(temp);
    }

    return(
        <>
        <div className="container">
            <div className="row">
                <Modal isOpen={abierto}>
                <ModalHeader>
                <h3 className="text-center">Creación de Proyecto</h3>
                </ModalHeader>
                <ModalBody>
                <div className="col-md-6 offset-md-3 offset-md-3">
                    <div className="card-body">
                        <form onSubmit={guardar}>
                            <div className="form-group">
                                <label>Descripcion del Proyecto: *</label>
                                <input placeholder="Nombre para el proyecto" name="descripcion" className="form-control" value={proyecto.descripcion} onChange={handleChange} />
                                <br></br>
                            </div>
                            <div className="form-group">
                                <label>Observaciones:</label>
                                <textarea name="observacion" className="form-control"  onChange={handleChange} >{proyecto.descripcion}</textarea>
                                <br></br>
                            </div>
                            <div className="form-group">
                                <label>Estado del Proyecto: *</label>
                                <select className="form-select" aria-label="Default select example" name="estado" >
                                    <option value='PENDIENTE'>Pendiente</option>
                                    <option value='PROCESO'>Proceso</option>
                                    <option value="FINALIZADO">Finalizado</option>
                                </select>
                                <br></br>
                                <br></br>
                            </div>
                            <input type='submit' className="btn btn-success"  value ='guardar'/>
                            <button className="btn btn-danger" onClick={funcCerrar} style={{marginLeft:"25px"}}>Cerrar</button>
                        </form>
                    </div>
                </div>
                </ModalBody>
                </Modal>
            </div>
        </div>
        </>
    )
}

export default FormProyecto
