import React,{useState,useEffect, memo} from 'react'
import { listaProyecto } from '../../actions/proyecto';
import { Slidebar } from '../slidebar/Slidebar';
import FilaProyecto from './FilaProyecto'
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'
import FormProyecto from './FormProyecto';
import { fetchGuardar } from '../../helpers/fetch';


//import React from 'react' // IMPORTAR DE AQUI EL FETCH CON TOKEN

export const Panel = memo(({setMenu}) => {
    const [modal, setModal] = useState();
    const [modoForm,setModoForm] = useState('N');
    const [lista,setLista] = useState({'lista':[]});
    const [select,setSelect] = useState({'id':'0'});
    const MySwal = withReactContent(Swal)

    const buscar= async (e)=>{
        e.preventDefault()
        const buscar = e.target[0].value
        setSelect({"id":'0'});
        console.log(select);
        listar(buscar );
    }
    const listar = async (valor)=>{
        const datos = await listaProyecto(valor);
        setLista(datos)
    }

    useEffect(()=>{
        listar();
    },[]);
    const seleccionarFila = (id)=>{
        setSelect({"id":id});
        console.log('id',id);

    }
    const cerrarModal = () => {
        setModal(0)
    }
    const nuevoModal = ()=>{
        setModoForm('N')
        setModal(1);
    }
    const modificarModal = ()=>{
        setModoForm('M')
        console.log(select)
        if(select.id!=='0'){
            setModal(1);
        }
    }
    const eliminarAlert = ()=>{
        setModal(0);
        fetchGuardar("proyecto/"+select.id,{},"E")
    }

    const sweetAlertEliminar = () => {
        if(select.id!=='0'){
            Swal.fire({
                title: 'Esta seguro que desea eliminar?',
                showDenyButton: true,  showCancelButton: true,
                confirmButtonText: `Confirmar`,
                denyButtonText: `No confirmar`,
              }).then((result) => {
                  /* Read more about isConfirmed, isDenied below */
                  if (result.isConfirmed) {
                      Swal.fire('Eliminado Correctamente!', '', 'success')
                      eliminarAlert();
                      window.location.href = window.location.href;
                  }
              });

        }

    }

    return (
        <>
            <div className="container-fluid">
                <div className="row">
                    <h1 style={{color:"white"}}>Proyecto</h1>
                </div>
                <div className="row">
                    <div className="col-sm-6">
                        <form onSubmit={buscar}>
                            <div className="row">
                                <div className="col-sm-8">
                                    <input type="text" className="form-control" placeholder="Buscar..."/>
                                </div>
                                <div className="col-sm-4">
                                    <button className="btn-primary btn" type='submit'>Buscar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div className="col-sm-6 text-end">
                        <button className="btn btn-success" onClick={()=>{nuevoModal()}}>Nuevo</button>
                        <button className="btn btn-secondary" onClick={()=>{modificarModal()}}>Modificar</button>
                        <button className="btn btn-danger" onClick={()=>{sweetAlertEliminar()}}>Eliminar</button>
                    </div>
                </div>
                <div className="row" style={{paddingTop:"5px"}}>
                    <hr/>
                </div>
                <div className="row">
                    <table className="table table-striped table-hover" style={{backgroundColor:"#ffffff"}}>
                        <thead>
                            <tr style={{backgroundColor:'darkblue',color:'white'}}><th>Descripcion</th><th>Estado</th></tr>
                        </thead>
                        <tbody>
                            {lista.lista.map(
                                    (fila,pos)=>{
                                        return (<tr key={fila.id+"f"} onClick={()=>{seleccionarFila(fila.id)}}><FilaProyecto key={fila.id} descripcion={fila.descripcion} estado={fila.estado}  /> boton</tr>);
                                    })
                            }
                        </tbody>
                    </table>
                </div>
                { (modal===1) && <FormProyecto modo={modoForm} seleccionado={select.id}  funcCerrar = {cerrarModal}/> }
            </div>
        </>
  )
})
