import React from 'react'
import PropTypes from 'prop-types'

const FilaProyecto = ({descripcion,estado}) => {
  return (
        <><td>{descripcion}</td><td>{estado}</td></>
  )
}

FilaProyecto.propTypes = {
    descripcion: PropTypes.string,
    estado: PropTypes.string,

}

export default FilaProyecto
