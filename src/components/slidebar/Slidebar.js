import React, { useEffect , memo} from 'react'
import { useSelector,useDispatch } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { rolesList } from '../../actions/roles';



export const Slidebar = memo(({setMenu}) => {
  const dispatch = useDispatch();


    dispatch( rolesList() );


  
  
  const { roles } = useSelector( state => state.roles);
  return (
    <div id="layoutSidenav" className='d-flex flex-column flex-shrink-0 p-3 text-white bg-dark' >
    <div id="layoutSidenav_nav">
        <nav className="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
            <div className="sb-sidenav-menu">
                <div className="nav">
              { roles.map( ({id, descripcion})  => {

              return (
                
                <button
                  className='btn-dark'
                  
                  key={id}
                  onClick={() => setMenu(id)}
                 
                >
                  {descripcion}
                  <div className="sb-nav-link-icon"><i className="fas fa-tachometer-alt"></i></div>

                </button>
                
                

              )

              })

              }
                </div>
            </div>

        </nav>
    </div>
</div>


  )
});
