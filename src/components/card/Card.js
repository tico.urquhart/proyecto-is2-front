import React from "react";
import PropTypes from 'prop-types'
import { NavLink } from "react-router-dom";


const  Card= ({nombre,observacion, setMenu}) => {

  const cambiar = () =>{
    if ( nombre === 'Usuarios') {
      setMenu(11);
    }else if ( nombre === 'Roles') {
      setMenu(12);
    }else if ( nombre === 'Modulos') {
      setMenu(13);
    }
  }
  return (
    <div className="col-xl-3 col-sm-6 col-12">
      <div className="card" style={{width: '15rem'}}>

        <div className="card-body" style={{textAlign: 'center', cursor: 'pointer'}}
        onClick={ () => cambiar()}
        >
          

            <h1 >{nombre}</h1>

          


        </div>
      </div>
    </div>
  )
}

Card.propTypes = {
  nombre : PropTypes.string,
  observacion : PropTypes.string
}

export default Card;
