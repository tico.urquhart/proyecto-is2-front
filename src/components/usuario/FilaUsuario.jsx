import React from 'react'
import PropTypes from 'prop-types'

const FilaUsuario = ({nombre,rol,email}) => {
  return (
        <><td>{nombre}</td><td>{email}</td><td>{rol}</td></>
  )
}

FilaUsuario.propTypes = {
    nombre: PropTypes.string,
    rol: PropTypes.string,
    email: PropTypes.string
}

export default FilaUsuario
