import React, { Component, useState } from "react";
import 'bootstrap/dist/css/bootstrap.css';
import { Button, Modal, ModalHeader, ModalBody } from "reactstrap";
import { fetchConToken,fetchObtenerDatos,fetchGuardar } from "../../helpers/fetch";
import { MultiSelect } from "react-multi-select-component";


class FormUsuario extends Component {

    constructor(props) {
        super(props)
        console.log(props)
        this.cerrar = this.props.funcCerrar;
        this.seleccionado = this.props.seleccionado;
        this.modo = this.props.modo;
        // this.listaRoles = [{'id':'1','descripcion':'Seguridad'},{'id':'2','descripcion':'Proyecto'},{'id':'3','descripcion':'Desarrollo'}];
        this.state={
            NombreUsuario:'',
            email:'',
            contra:'',
            ConfirContra:'',
            roles:[],
            abierto: true,
            listaRoles : [{'id':'1','descripcion':'Seguridad'},{'id':'2','descripcion':'Proyecto'},{'id':'3','descripcion':'Desarrollo'}]
        }
        this.getListaRoles();
        this.changeNombreUsuarioHandler=this.changeNombreUsuarioHandler.bind(this);
        this.changeemailHandler=this.changeemailHandler.bind(this);
        this.changecontraHandler=this.changecontraHandler.bind(this);
        this.changeConfirContraHandler=this.changeConfirContraHandler.bind(this);
        this.guardar=this.guardar.bind(this);
        if(this.modo ==='M') this.obtenerDatos()
    }

    obtenerDatos = async ()=>{
        console.log("Testing Modificar");
        let temp = await (await fetchObtenerDatos('usuario/id',this.seleccionado)).json()
        console.log(temp);
        this.setState({
            NombreUsuario : temp.usuario.name,
            email:temp.usuario.email,
            rol:temp.usuario.rol
        })
        return "";
    }
    abrirModal=()=>{
        this.setState({
            abierto: !this.state.abierto
        });
    }

    changeNombreUsuarioHandler=(event)=>{
        this.setState({
            NombreUsuario: event.target.value
        })
    }

    changeListaModulos = (lista)=>{
        this.listaModulos = lista
    }

    changeemailHandler=(event)=>{
        this.setState({
            email: event.target.value
        })
    }

    changecontraHandler=(event)=>{
        this.setState({
            contra: event.target.value
        })
    }

    changeConfirContraHandler=(event)=>{
        this.setState({
            ConfirContra: event.target.value
        })
    }

    changerolHandler=(event)=>{
        this.setState({
            rol: event.target.value
        })
    }

    guardar = async (e)=>{
        e.preventDefault();
        let temp ;
        let usuario={
        	"nombre":this.state.NombreUsuario,
        	"email":this.state.email,
        	"pass":this.state.contra,
        	"pass_confirmation":this.state.ConfirContra,
            "id_rol":this.state.rol
        }
        console.log('usuario=>'+JSON.stringify(usuario));
        if(this.modo==='M'){
            temp= await (await fetchGuardar('usuario/'+this.seleccionado,usuario , this.modo)).json()

        }else{
            console.log(this.modo);
            temp= await (await fetchGuardar('usuario',usuario , this.modo)).json()

        }
        console.log(temp);
        alert('Usuario creado con éxito!')
    }

    getListaRoles = async () =>{
        let temp = await (await fetchObtenerDatos('rol')).json()
        console.log(temp);
        this.setState({
            listaRoles:temp.lista
        });
    }
    render() {
        return(
            <div>
            <div className="container">
                <div className="row">
                    <Modal isOpen={this.state.abierto}>
                    <ModalHeader>
                    <h3 className="text-center">Creación de usuario</h3>
                    </ModalHeader>
                    <ModalBody>
                    <div className="col-md-8 offset-md-3 offset-md-3">
                            <form>
                                <div className="form-group">
                                    <label>Nombre de Usuario: *</label>
                                    <input placeholder="Nombre de usuario" name="NombreUsuario" className="form-control" value={this.state.NombreUsuario} onChange={this.changeNombreUsuarioHandler}/>
                                    <br></br>
                                </div>
                                <div className="form-group">
                                    <label>E-mail: *</label>
                                    <input placeholder="nombre@ejemplo.com" name="email" className="form-control" value={this.state.email} onChange={this.changeemailHandler}/>
                                    <br></br>
                                </div>
                                <div className="form-group">
                                    <label>Contraseña: *</label>
                                    <input type={"password"} name="contra" className="form-control" value={this.state.contra} onChange={this.changecontraHandler}/>
                                    <div id="passwordHelpBlock" class="form-text">
                                        La contraseña debe ser de por lo menos 8 caracteres (solo letras y numeros)
                                    </div>
                                    <br></br>
                                </div>
                                <div className="form-group">
                                    <label>Confirmar contraseña: *</label>
                                    <input type={"password"} name="ConfirContra" className="form-control" value={this.state.ConfirContra} onChange={this.changeConfirContraHandler}/>
                                    <br></br>
                                </div>
                                <div className="form-group">
                                    <label>Rol(es): *</label>

                                    <select className="form-select" aria-label="Default select example" value={this.state.rol} onChange={this.changerolHandler}>

                                        {this.state.listaRoles.map((fila,pos)=>{ return <option value={fila.id} >{fila.descripcion}</option>})}
                                    </select>

                                    <br></br>
                                </div>
                                <button className="btn btn-success" onClick={this.guardar}>Guardar</button>
                                <button className="btn btn-danger" onClick={this.cerrar} style={{marginLeft:"25px"}}>Cerrar</button>
                            </form>
                    </div>
                    </ModalBody>
                    </Modal>
                </div>
            </div>
            </div>
        )
    }
}

export default FormUsuario
