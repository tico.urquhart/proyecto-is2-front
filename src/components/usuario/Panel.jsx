import React,{useState,useEffect} from 'react'
import { listaUsuarios } from '../../actions/usaurios';
import { Slidebar } from '../slidebar/Slidebar';
import FilaUsuario from './FilaUsuario'
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'
import FormUsuario from './FormUsuario';
import { fetchGuardar } from '../../helpers/fetch';


//import React from 'react' // IMPORTAR DE AQUI EL FETCH CON TOKEN

export const Panel = () => {
    const [modal, setModal] = useState();
    const [modoForm,setModoForm] = useState('N');
    const [lista,setLista] = useState({'lista':[]});
    const [select,setSelect] = useState({'id':'0'});
    const MySwal = withReactContent(Swal)

    const buscar= async (e)=>{
        e.preventDefault()
        const buscar = e.target[0].value
        setSelect({"id":'0'});  
        console.log(select);
        listar(buscar );
    }
    const listar = async (valor)=>{
        const datos = await listaUsuarios(valor);
        setLista(datos)
    }

    useEffect(()=>{
        listar();
    },[]);
    const seleccionarFila = (id)=>{
        setSelect({"id":id});
        console.log('id',id);

    }
    const cerrarModal = () => {
        setModal(0)
    }
    const nuevoModal = ()=>{
        setModoForm('N')
        setModal(1);
    }
    const modificarModal = ()=>{
        setModoForm('M')
        console.log(select)
        if(select.id!=='0'){
            setModal(1);
        }
    }
    const eliminarAlert = ()=>{
        setModal(0);
        fetchGuardar("usuario/"+select.id,{},"E")
    }

    const sweetAlertEliminar = () => {
        if(select.id!=='0'){
            Swal.fire({
                title: 'Esta seguro que desea eliminar?',
                showDenyButton: true,  showCancelButton: true,
                confirmButtonText: `Confirmar`,
                denyButtonText: `No confirmar`,
              }).then((result) => {
                  /* Read more about isConfirmed, isDenied below */
                  if (result.isConfirmed) {
                      Swal.fire('Eliminado Correctamente!', '', 'success')
                      eliminarAlert();
                      window.location.href = window.location.href;
                  }
              });
            
        }

    }

    return (
        <>
            <div className="container-fluid">
                <div className="row">
                    <h1 style={{color:"white"}}>Usuario</h1>
                </div>
                <div className="row">
                    <div className="col-sm-6">
                        <form onSubmit={buscar}>
                            <div className="row">
                                <div className="col-sm-8">
                                    <input type="text" className="form-control" placeholder="Buscar..."/>
                                </div>
                                <div className="col-sm-4">
                                    <button className="btn-primary btn" type='submit'>Buscar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div className="col-sm-6 text-end">
                        <button className="btn btn-success" onClick={()=>{nuevoModal()}}>Nuevo</button>
                        <button className="btn btn-secondary" onClick={()=>{modificarModal()}}>Modificar</button>
                        <button className="btn btn-danger" onClick={()=>{sweetAlertEliminar()}}>Eliminar</button>
                    </div>
                </div>
                <div className="row" style={{paddingTop:"5px"}}>
                    <hr/>
                </div>
                <div className="row">
                    <table className="table table-striped table-hover" style={{backgroundColor:"#ffffff"}}>
                        <thead>
                            <tr style={{backgroundColor:'darkblue',color:'white'}}><th>Nombre</th><th>Correo</th><th>Rol</th></tr>
                        </thead>
                        <tbody>
                            {lista.lista.map(
                                    (fila,pos)=>{
                                        return (<tr key={fila.id+"f"} onClick={()=>{seleccionarFila(fila.id)}}><FilaUsuario key={fila.id} nombre={fila.name} rol={fila.rol} email={fila.email} /></tr>);
                                    })
                            }
                        </tbody>
                    </table>
                </div>
                { (modal===1) && <FormUsuario modo={modoForm} seleccionado={select.id}  funcCerrar = {cerrarModal}/> }
            </div>
        </>
  )
}
