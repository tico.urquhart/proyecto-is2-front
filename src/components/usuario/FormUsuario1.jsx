import React, { useState } from "react";
import 'bootstrap/dist/css/bootstrap.css';
import { Button, Modal, ModalHeader, ModalBody } from "reactstrap";
import { MultiSelect } from "react-multi-select-component";


const FormUsuario1 = () => {

    const [usuario, setUsuario] = useState({
        NombreUsuario:'',
        email:'',
        contra:'',
        ConfirContra:''
    })

    const [roles, setRoles] = useState([]);

    const [abierto, setAbierto] = useState(true)

    const listaRoles = [
        {label:'Seguridad', value:1},
        {label:'Proyecto', value:2},
        {label:'Desarrollo', value:3}]

    const changeUserHandler = (event) =>{
        setUsuario({
            ...usuario,
            [event.target.name]:event.target.value
        })
    }

    const abrirModal=()=>{
        setAbierto({
            abierto: !abierto
        });
    }

    const guardar = async (e)=>{
        e.preventDefault();
        //let temp ;
        let idRoles = []
         for (let i = 0; i < roles.length; i++) {
            idRoles.push(roles[i].value)
        }
        console.log('Nombre:'+usuario.NombreUsuario+' email:'+usuario.email+' pass:'+usuario.contra+' roles:'+idRoles)
        /*if(modo==='M'){
            temp= await (await fetchGuardar('usuario/'+seleccionado,usuario , modo)).json()

        }else{
            console.log(modo);
            temp= await (await fetchGuardar('usuario',usuario , modo)).json()

        }
        console.log(temp);*/
        alert('Usuario creado con éxito!')
    }

    const cancel=()=>{
        this.props.history.push();
    }
    
    return(
        <div>
        <div className="container">
            <div className="row">
                <Modal isOpen={abierto}>
                <ModalHeader>
                <h3 className="text-center">Creación de usuario</h3>
                </ModalHeader>
                <ModalBody>
                <div className="col-md-8 offset-md-3 offset-md-3">
                        <form>
                            <div className="form-group">
                                <label>Nombre de Usuario: *</label>
                                <input placeholder="Nombre de usuario" name="NombreUsuario" className="form-control" onChange={changeUserHandler}/>
                                <br></br>
                            </div>
                            <div className="form-group">
                                <label>E-mail: *</label>
                                <input placeholder="nombre@ejemplo.com" name="email" className="form-control" onChange={changeUserHandler}/>
                                <br></br>
                            </div>
                            <div className="form-group">
                                <label>Contraseña: *</label>
                                <input type={"password"} name="contra" className="form-control" onChange={changeUserHandler}/>
                                <div id="passwordHelpBlock" class="form-text">
                                    La contraseña debe ser de por lo menos 8 caracteres (solo letras y numeros)
                                </div>
                                <br></br>
                            </div>
                            <div className="form-group">
                                <label>Confirmar contraseña: *</label>
                                <input type={"password"} name="ConfirContra" className="form-control" onChange={changeUserHandler}/>
                                <br></br>
                            </div>
                            <div className="form-group">
                                <label>Rol(es): *</label>

                                <MultiSelect
                                    hasSelectAll={false}
                                    options={listaRoles}
                                    value={roles}
                                    onChange={setRoles}
                                    labelledBy="Select"
                                />

                                <br></br>
                            </div>
                            <button className="btn btn-success" onClick={guardar}>Guardar</button>
                            <button className="btn btn-danger" onClick={cancel} style={{marginLeft:"25px"}}>Cerrar</button>
                        </form>
                </div>
                </ModalBody>
                </Modal>
            </div>
        </div>
        </div>
    )

}

export default FormUsuario1