import React from 'react';
import { Container, Form, Navbar } from 'react-bootstrap';

import { useDispatch } from 'react-redux';
import { useForm } from '../../hooks/useForm';
import { startLogin } from '../../actions/auth';

import './login.css'



export const LoginScreen = () => {

    const dispatch = useDispatch();

    
    const [ formLoginValues, handleLoginInputChange ] = useForm({
        lEmail: 'mai@mail.com',
        lPassword: '123456'
    });

    
    const { lEmail, lPassword } = formLoginValues;
   

    const handleLogin = ( e ) => {
        e.preventDefault();
        dispatch( startLogin( lEmail, lPassword ) );
    }

    return (
        <div className="container">
        <div className="row">
          <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <div className="card border-0 shadow rounded-3 my-5">
              <div className="card-body p-4 p-sm-5">
                <h5 className="card-title text-center mb-5 fw-light fs-5">Sign In</h5>
                <form>
                    <div className="form-group ">
                        <label>Email address</label>
                        <input className="form-control" 
                        type="text"
                        placeholder="Correo"
                        name="lEmail"
                        value={ lEmail }
                        onChange={ handleLoginInputChange }
                        />
                        </div>

                        <div className="form-group ">
                        <label>password</label>
                        <input type="password"
                        className="form-control"
                        placeholder="Contraseña"
                        name="lPassword"
                        value={ lPassword }
                        onChange={ handleLoginInputChange }
                        />
                    </div>

                  <div className="d-grid">
                    <button 
                        type="submit"
                        className="btn btn-primary btn-block"
                        onClick={handleLogin} 

                    >
                      <i className="fab fa-facebook-f me-2"></i> Sign in
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
     );
}