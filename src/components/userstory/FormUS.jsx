import React, {useState } from "react";
import 'bootstrap/dist/css/bootstrap.css';
import { Button, Modal, ModalHeader, ModalBody } from "reactstrap";

const FormUS = () => {
    
    const [abierto, setAbierto] = useState(true)

    const [US, setUS] = useState({
        titulo:'',
        desc:'',
        puntos:0
    })

    const abrirModal=()=>{
        setAbierto({
            abierto: !abierto
        });
    }

    const changeUSHandler = (event) => {
        setUS({
            ...US,
            [event.target.name]:event.target.value
        })
    }

    const guardar = async (e)=>{
        e.preventDefault();
        //let temp ;
        console.log('titulo:'+US.titulo+' desc:'+US.desc+' puntos:'+US.puntos)
        /*if(modo==='M'){
            temp= await (await fetchGuardar('usuario/'+seleccionado,usuario , modo)).json()

        }else{
            console.log(modo);
            temp= await (await fetchGuardar('usuario',usuario , modo)).json()

        }
        console.log(temp);*/
        alert('US creado con éxito!')
    }

    const cancel=()=>{
        this.props.history.push();
    }
    
    return(
            <div>
            <div className="container">
                <div className="row">
                    <Button color="success" onClick={abrirModal}>Crear nueva UserStory</Button>
                    <Modal isOpen={abierto}>
                    <ModalHeader>
                    <h3 className="text-center">Creación de UserStory</h3>
                    </ModalHeader>
                    <ModalBody>
                    <div className="col-md-6 offset-md-3 offset-md-3">
                        <div className="card-body">
                            <form>
                                <div className="form-group">
                                    <label>Título: *</label>
                                    <br></br>
                                    <input placeholder="Titulo para US" name="titulo" className="form-control" onChange={changeUSHandler}/>
                                    <br></br>
                                </div>
                                <div className="form-group">
                                    <label>Descripción: *</label>
                                    <textarea name="desc" className="form-control" onChange={changeUSHandler}></textarea>
                                    <br></br>
                                </div>
                                <div className="form-group">
                                    <label>Puntos *</label>
                                    <br></br>
                                    <input type='number' placeholder="0" name="puntos" className="form-control" onChange={changeUSHandler}/>
                                    <br></br>
                                </div>
                                <button className="btn btn-success" onClick={guardar}>Guardar</button>
                                <button className="btn btn-danger" onClick={cancel} style={{marginLeft:"25px"}}>Cerrar</button>
                            </form>
                        </div>
                    </div>
                    </ModalBody>
                    </Modal>
                </div>
            </div>   
            </div>
        )
}

export default FormUS