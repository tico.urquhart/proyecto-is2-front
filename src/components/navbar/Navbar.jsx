import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { useDispatch } from 'react-redux';
import { startLogout } from '../../actions/auth';

const Navbar = () => {
    
    const dispatch = useDispatch();

    const {name} = useSelector( state => state.auth );

    const handleLogout = () => {
        dispatch( startLogout() );
    }
    
  return (
<nav className="sb-topnav navbar navbar-expand navbar-dark bg-dark">
          
            <a className="navbar-brand ps-3" href="index.html">Jira 3.0</a>
            <ul className="navbar-nav d-none d-md-inline-block form-inline ms-auto ">
                <li className="nav-item dropdown">
                    <a className="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        <i className="fas fa-user fa-fw">{name}</i></a>
                    <ul className="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                        <li><a className="dropdown-item" onClick={handleLogout}>Logout</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
  )
}

export default Navbar
