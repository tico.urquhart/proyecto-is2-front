import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { useDispatch } from 'react-redux';
import { startLogout } from '../../actions/auth';

const Navbar = () => {
    const dispatch = useDispatch();

    const {name} = useSelector( state => state.auth );

    const handleLogout = () => {
        dispatch( startLogout() );
    }
    
  return (

              <nav className="navbar navbar-dark bg-dark">
                  <h3 className="navbar-brand" >JIRA v0.3</h3>

                  <div className="dropdown">
                      <button 
                        className="btn btn-secondary dropdown-toggle" 
                        id="dropdownMenuLink"
                        data-bs-toggle="dropdown" 
                        aria-expanded="false"
                         >
                          {name}
                      </button>
                      <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      <li><a className="dropdown-item" onClick={handleLogout}>Logout</a></li>
                      </div>
                  </div>

              </nav>
  )
}

export default Navbar
