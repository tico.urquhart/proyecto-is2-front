import { useState } from "react"


export const useMenu = ( initialState) => {
    const [menu, setMenu] = useState(initialState);

    const reset = () => {
        setMenu( initialState );
    };
    return { menu, setMenu, reset };
}