import React, { Component, useEffect } from 'react';
import { Route,  BrowserRouter as Router, Routes } from 'react-router-dom';

import { useDispatch, useSelector } from 'react-redux';

import { startChecking } from '../actions/auth';
import { PublicRoute } from './PublicRoute';
import { PrivateRoute } from './PrivateRoute';
import { LoginScreen } from '../components/auth/LoginScreen';
import { Home } from '../components/home/Home';
import { Panel as PanelUsuario } from '../components/usuario/Panel';
import { Panel as PanelRol } from '../components/rol/Panel';
import { Panel as PanelModulo} from '../components/modulo/Panel';
import FormUsuario from '../components/usuario/FormUsuario';
import FormRol from '../components/rol/FormRol';
import { Seguridad } from '../components/seguridad/Seguridad';

export const AppRouter = () => {

    const dispatch = useDispatch();
    const { checking, uid } = useSelector( state => state.auth);
    //console.log(!!uid)

    useEffect(() => {

        dispatch( startChecking() );

    }, [dispatch])

    if( checking ){
        return ( <h5> Espere... </h5> )
      }
      return (
        <Router>
          <div>
            <Routes>
              <Route exact path='/login' element={
                  <PublicRoute uid={uid} >
                    <LoginScreen/>
                  </PublicRoute>
                }
              />
          <Route exact path='/proyecto'
                element={
                  <PrivateRoute uid={uid}>
                    <PanelRol />
                  </PrivateRoute>
                }
              />
              <Route exact path='/usuarios'
                element={
                  <PrivateRoute uid={uid}>
                    <PanelUsuario />
                  </PrivateRoute>
                }
              />
              <Route exact path='/roles'
                element={
                  <PrivateRoute uid={uid}>
                    <PanelRol />
                  </PrivateRoute>
                }
              />

              <Route exact path='/modulos'
                element={
                  <PrivateRoute uid={uid}>
                    <PanelModulo />
                  </PrivateRoute>
                }
              />

              <Route exact path='/*'
                element={
                  <PrivateRoute uid={uid}>
                    <Home />
                  </PrivateRoute>
                }
              />

            </Routes>
          </div>
        </Router>
      )
}
