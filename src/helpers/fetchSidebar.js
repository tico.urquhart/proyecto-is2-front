export const fetchSidebar = () => {

    const getUser = async (url, credenciales) => {
      const respuesta = await fetch( url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(credenciales)
        } );
       const resp = await respuesta.json();

       return resp;

    }
    return {getUser};
}
