


export const fetchUser = () => {

    const getUser = async (url, credenciales) => {
        const respuesta = await fetch( url, {
            method: 'GET',
            headers: {
                'Accept':'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(credenciales)
        } );
        const resp = await respuesta.json();

        return resp;

    }
    return {getUser};
}
