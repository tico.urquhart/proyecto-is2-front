

const baseUrl = process.env.REACT_APP_API_URL;

const fetchSinToken = ( endpoint, data, method = 'GET' ) => {

    const url = `${ baseUrl }/${ endpoint }`;


    if ( method === 'GET' ) {
        return fetch( url );
    } else {
        return fetch( url, {
            method,
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify( data )
        });
    }
}

const fetchConToken = ( endpoint, data, method = 'GET' ) => {

    const url = `${ baseUrl }/${ endpoint }`;
    const token = localStorage.getItem('token') || '';

    if (  (endpoint === 'login') ) {

        if ( method === 'GET' ) {
            return fetch( url, {
                method,
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
        } else {
            return fetch( url, {
                method,
                headers: {
                    'Content-type': 'application/json',
                    'Authorization': 'Bearer ' + token
                },
                body: JSON.stringify( data )
            });
        }
    } else {
        switch (endpoint) {
            case 'auth/renew':
                return {
                    cod: (token) ? '00' : '01',
                    token: token,
                    uid: 1,
                    name: 'administrador'
                }
                break;
            case 'usuario':
                return fetch( url, {
                    method,
                    headers: {
                        'Content-type': 'application/json',
                        'Authorization': 'Bearer ' + token
                    }
                });
                break;
            case 'rol':
                return fetch( url, {
                    method,
                    headers: {
                        'Content-type': 'application/json',
                        'Authorization': 'Bearer ' + token
                    }
                });
                break;
            case 'logueadoModulos':
                return fetch( url, {
                    method,
                    headers: {
                        'Content-type': 'application/json',
                        'Authorization': 'Bearer ' + token
                    }
                });
                break;
                case 'modulos':
                    return fetch( url, {
                        method,
                        headers: {
                            'Content-type': 'application/json',
                            'Authorization': 'Bearer ' + token
                        }
                    });
                    break;
            default:
                return {}

        }
    }

}


const fetchObtenerDatos = ( endpoint, valor="" ) => {
    console.log(valor)
    const url = `${ baseUrl }/${ endpoint }/${valor}`;
    const token = localStorage.getItem('token') || '';
    const method = 'GET';
    console.log(url);
    return fetch( url, {
        method,
        headers: {
            'Content-type': 'application/json',
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + token
        }
    });
}

const fetchGuardar = ( endpoint, objValores , modo = 'N' ) => {
    const url = `${ baseUrl }/${ endpoint }`;
    const token = localStorage.getItem('token') || '';

    if(modo === 'N'){
        const method = 'POST';
        return fetch( url, {
            method,
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + token
            },
            body: JSON.stringify( objValores )
        });
    }else if( modo==='M'){
        const method = 'PUT';
        return fetch( url, {
            method,
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + token
            },
            body: JSON.stringify( objValores )
        });
    }
    else if( modo==='E'){
        const method = 'DELETE';
        return fetch( url, {
            method,
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + token
            },
            body: JSON.stringify( objValores )
        });
    }
}

export {
    fetchSinToken,
    fetchConToken,
    fetchObtenerDatos,
    fetchGuardar
}
